#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>

int main()
{
	struct ifreq ifr;			/* CAN interface info struct */
	struct sockaddr_can addr;	/* CAN adddress info struct */
	struct can_frame frame;		/* CAN frame struct */
	int s;						/* SocketCAN handle */

	int command_counter=0;
	
	memset(&ifr, 0, sizeof(ifr));
	memset(&addr, 0, sizeof(addr));
	memset(&frame, 0, sizeof(frame));
	
	s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

	strcpy(ifr.ifr_name, "vcan0");
	ioctl(s, SIOCGIFINDEX, &ifr);
	
	addr.can_ifindex = ifr.ifr_ifindex;
	addr.can_family = AF_CAN;
	
	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);
	
	bind(s, (struct sockaddr *)&addr, sizeof(addr));

	frame.can_id = 0x128;
    frame.can_dlc = 4;
    frame.data[0] = 0xAB;
    frame.data[1] = 0xBA;
    frame.data[2] = 0xDA;
    frame.data[3] = 0xFA;

    while(1)
    {
		if(command_counter<10)
		{
			command_counter++;
		}
		else
		{
			sleep(10);
			command_counter=0;
		}
        write(s, &frame, sizeof(frame));
        sleep(0.5);
    }

	close(s);
	
	return 0;
}

