#!/bin/sh
#kreiranje htb qdisca-a unutar root-a
tc qdisc add dev vcan0 root handle 1: htb
#kreiranje klasa u okviru htb qdisc-a sa raspodjelom propusnog opsega
tc class add dev vcan0 parent 1: classid 1:1 htb rate 500kbps ceil 500kbps
tc class add dev vcan0 parent 1:1 classid 1:10 htb rate 150kbps ceil 500kbps
tc class add dev vcan0 parent 1:1 classid 1:11 htb rate 150kbps ceil 500kbps
tc class add dev vcan0 parent 1:1 classid 1:12 htb rate 300kbps ceil 500kbps
#dodavanje child qdsic-ova u klase htb qdisc-a
tc qdisc add dev vcan0 parent 1:10 tbf rate 150bit burst 640b latency 100ms
tc qdisc add dev vcan0 parent 1:11 tbf rate 150bit burst 640b latency 100ms
tc qdisc add dev vcan0 parent 1:12 tbf rate 150bit burst 640b latency 100ms
#dodavanje filtera za svaku od klasa
tc filter add dev vcan0 parent 1: prio 1 u32 match u32  0x00000001 0x0000000f at 0 flowid 1:1
tc filter add dev vcan0 parent 1:1 prio 2 u32 match u32  0x00000921 0xffffffff at 0 flowid 1:10
tc filter add dev vcan0 parent 1:1 prio 3 u32 match u32  0x00000821 0xffffffff at 0 flowid 1:11
tc filter add dev vcan0 parent 1:1 prio 4 u32 match u32  0x00000031 0xffffffff at 0 flowid 1:12
